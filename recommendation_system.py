import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import numpy as np

# Charger les données des films
movies = pd.read_csv('movies.csv')

# Vérifier le type de données de la colonne 'title'
print(movies['title'].dtype)

# Si le type de données n'est pas une chaîne de caractères, convertissez-le en chaîne de caractères
movies['title'] = movies['title'].astype(str)

# Charger les données des notations des utilisateurs
ratings_data = pd.read_csv('ratings_small.csv', usecols=['userId', 'movieId', 'rating'])

# Division des données
train_data, test_data = train_test_split(ratings_data, test_size=0.2, random_state=42)

# Création de la matrice de notation utilisateur-film
user_movie_matrix = train_data.pivot_table(index='userId', columns='movieId', values='rating')

# Calcul des similarités entre utilisateurs en utilisant la corrélation de Pearson
user_similarity = user_movie_matrix.corr(method='pearson')

# Définir une fonction pour obtenir des films similaires
def get_similar_movies(movie_name, top_n=5):
    movie = movies[movies['title'].str.contains(movie_name, case=False)]
    if len(movie) == 0:
        return "Aucun film trouvé avec ce nom."
    
    # Obtenir les genres du film
    movie_genres = movies[movies['title'].str.contains(movie_name, case=False)]['genre'].iloc[0]
    
    # Filtrer les films similaires par genre
    similar_movies = movies[movies['genre'].apply(lambda x: any(g in x for g in movie_genres.split('|')))]
    
    # Exclure le film lui-même de la liste des recommandations
    similar_movies = similar_movies[~similar_movies['title'].str.contains(movie_name, case=False)]
    
    # Obtenir les titres des films recommandés
    similar_movie_titles = similar_movies['title'].tolist()[:top_n]
    
    return similar_movie_titles


# Définir une fonction pour prédire les évaluations des films
def predict_ratings(user_id, movie_id):
    if user_id in user_similarity and movie_id in user_movie_matrix.columns:
        similar_users = user_similarity[user_id].dropna()
        movie_ratings = user_movie_matrix[movie_id]
        similar_ratings = similar_users.map(lambda x: x * movie_ratings.get(x, 0))
        predicted_rating = similar_ratings.sum() / similar_users.sum()
        return predicted_rating
    else:
        return None

# Prédire les évaluations des films pour les utilisateurs dans l'ensemble de test
test_data['predicted_rating'] = test_data.apply(lambda x: predict_ratings(x['userId'], x['movieId']), axis=1)

# Remplacer les valeurs NaN par 0 dans la colonne 'predicted_rating'
test_data['predicted_rating'] = test_data['predicted_rating'].fillna(0)

# Calculer le RMSE
rmse = np.sqrt(mean_squared_error(test_data['rating'], test_data['predicted_rating']))
print("RMSE:", rmse)

# Utiliser la fonction pour obtenir des recommandations de films similaires
recommended_movies = get_similar_movies('The Godfather')
print(recommended_movies)



import requests

def get_movie_poster(movie_name):
    api_key = '9afe6e0b384703cb349dda8fa201e33a'
    search_url = f'https://api.themoviedb.org/3/search/movie'
    params = {'api_key': api_key, 'query': movie_name}
    response = requests.get(search_url, params=params)
    if response.status_code == 200:
        data = response.json()
        if data['total_results'] > 0:
            movie_id = data['results'][0]['id']
            poster_path = data['results'][0]['poster_path']
            base_url = 'https://image.tmdb.org/t/p/w500'
            return base_url + poster_path
    return None

# Utiliser la fonction pour obtenir l'URL de l'affiche d'un film recommandé
recommended_movies = get_similar_movies('The Godfather')
poster_urls = []
for movie in recommended_movies:
    poster_url = get_movie_poster(movie)
    if poster_url:
        poster_urls.append(poster_url)
    else:
        poster_urls.append('https://via.placeholder.com/500x750.png?text=Poster+Not+Available')

