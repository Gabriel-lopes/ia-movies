# app.py

from flask import Flask, request, render_template
from recommendation_system import get_movie_poster, get_similar_movies

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/recommend', methods=['POST'])
def recommend():
    if request.method == 'POST':
        movie_name = request.form['movie_name']
        similar_movies = get_similar_movies(movie_name)
        poster_urls = []
        for movie in similar_movies:
            poster_url = get_movie_poster(movie)
            if poster_url:
                poster_urls.append((movie, poster_url))
            else:
                poster_urls.append((movie, 'https://via.placeholder.com/500x750.png?text=Poster+Not+Available'))
        return render_template('recommendations.html', movie_name=movie_name, movies=poster_urls)

if __name__ == '__main__':
    app.run(debug=True)
